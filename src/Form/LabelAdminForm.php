<?php

namespace Drupal\labels\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class LabelAdminForm.
 *
 * @package Drupal\labels\Form
 */
class LabelAdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'label_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'labels.labels',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $label = NULL) {
    if ($label) {
      // Get labels as an array.
      $config = \Drupal::service('config.factory')->get('labels.labels')->getRawData();

      // Set default values.
      $defaultTheme = $config[$label]['themes'];
      $defaultHooks = $config[$label]['hooks'];
      $defaultName = $config[$label]['name'];
      $defaultLabel = $label;
      $labelDisabled = TRUE;
      $defaultReplacement = $config[$label]['replacement'];
      $op = 'edit';
    }
    else {
      // Get default theme from config. Set as default value.
      $defaultTheme = $this->config('system.theme')->get('default');

      // Set other default values.
      $defaultHooks = [];
      $defaultName = '';
      $defaultLabel = '';
      $defaultReplacement = '';
      $labelDisabled = FALSE;
      $op = 'add';
    }

    // Get all available themes as select option array.
    $themes = system_list('theme');
    $themeOptions = [];
    foreach ($themes as $id => $theme) {
      $themeOptions[$id] = $theme->info['name'];
    }

    // Get all available theme hooks as select option array.
    $hooks = theme_get_registry();
    $hookOptions = [];
    foreach ($hooks as $id => $hookInfo) {
      $hookOptions[$id] = $id;
    }
    ksort($hookOptions);

    // Form fields. All required.
    $form['name'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Name'),
      '#description' => $this->t('A human-friendly name for this label.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $defaultName,
    );
    $form['label'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#description' => $this->t('A machine-readable label. May only contain uppercase letters, lowercase letters, and underscores.'),
      '#maxlength' => 64,
      '#size' => 64,
      '#disabled' => $labelDisabled,
      '#required' => TRUE,
      '#default_value' => $defaultLabel,
    );
    $form['themes'] = array(
      '#type' => 'select',
      '#title' => $this->t('Themes'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $themeOptions,
      '#default_value' => $defaultTheme,
      '#description' => $this->t('Select the themes to provide this label to.'),
    );
    $form['hooks'] = array(
      '#type' => 'select',
      '#title' => $this->t('Theme Hooks'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#options' => $hookOptions,
      '#description' => $this->t('Limit this label to specific theme hooks. Choosing many theme hooks may result in a performance issue.'),
      '#default_value' => $defaultHooks,
    );
    $form['replacement'] = array(
      '#type' => 'textfield',
      '#title' => 'Replacement Text',
      '#description' => $this->t('This text will replace the label in any twig it is used'),
      '#maxlength' => 255,
      '#size' => 64,
      '#required' => TRUE,
      '#default_value' => $defaultReplacement,
    );
    $form['op'] = array(
      '#type' => 'hidden',
      '#value' => $op,
    );

    // Add default submit button and behaviors.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Get label field value as a string.
    $label = $form_state->getValue('label');

    // Make sure it meets the format requirements. Otherwise, throw error.
    if (!preg_match("/^[a-zA-Z_]+$/", $label)) {
      $form_state->setErrorByName('label', $this->t('Labels must only contain uppercase letters, lowercase letters, and underscores.'));
    }

    // Get config as object.
    $config = \Drupal::service('config.factory')->getEditable('labels.labels');

    if ($form['op']['#value'] == 'add' || ($form['op']['#value'] != 'edit' && $label == $form['label']['#default_value'])) {
      // Throw error if label already exists with this name.
      if ($config->get($label)) {
        $form_state->setErrorByName('label', $this->t('Label already exists.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get config as editable config object.
    $config = \Drupal::service('config.factory')->getEditable('labels.labels');

    // Add the label and label to the config list.
    $settings = [
      'name' => $form_state->getValue('name'),
      'replacement' => $form_state->getValue('replacement'),
      'themes' => $form_state->getValue('themes'),
      'hooks' => $form_state->getValue('hooks'),
    ];
    $config->set($form_state->getValue('label'), $settings)->save();

    // Set the success message and return to admin list.
    drupal_set_message(t('Label created successfully.'));
    $form_state->setRedirect('labels.admin_list');
  }

}
