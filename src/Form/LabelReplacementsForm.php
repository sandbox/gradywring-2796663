<?php

namespace Drupal\labels\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\labels\Controller\LabelController;

/**
 * Class LabelReplacementsForm.
 *
 * @package Drupal\labels\Form
 */
class LabelReplacementsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'labels_replacements_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'labels.labels',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Get labels as an array.
    $config = \Drupal::service('config.factory')->get('labels.labels')->getRawData();

    // Create a form field for each label.
    foreach ($config as $label => $data) {
      // Get a twig-safe label string.
      $twigSafeLabel = LabelController::getTwigSafeLabel($label);

      if (isset($data['replacement'])) {
        $replacement = $data['replacement'];
      }
      else {
        $replacement = '';
      }

      $form[$label] = [
        '#type' => 'textfield',
        '#title' => $data['name'],
        '#description' => '{{ ' . $twigSafeLabel . ' }}',
        '#default_value' => $replacement,
        '#maxlength' => 255,
        '#size' => 64,
        '#required' => TRUE,
      ];
    }

    // Add default submit button.
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the label config as a config object.
    $config = \Drupal::service('config.factory')->getEditable('labels.labels');

    // Get the labels as an array.
    $configData = $config->getRawData();

    // Save each label's replacement value.
    foreach ($configData as $label => $data) {
      $config->set($label, array('name' => $data['name'], 'replacement' => $form_state->getValue($label)))->save();
    }

    // Default submit behavior.
    parent::submitForm($form, $form_state);
  }

}
