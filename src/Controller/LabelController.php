<?php

namespace Drupal\labels\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Config\ConfigFactory;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class LabelController.
 *
 * @package Drupal\labels\Controller
 */
class LabelController extends ControllerBase {

  /**
   * ConfigFactory service.
   *
   * @var ConfigFactory
   */
  protected $configFactory;

  /**
   * Class constructor.
   */
  public function __construct(ConfigFactory $configFactory) {
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * AdminListLabels.
   *
   * @return array
   *   Return table results render array.
   */
  public function adminList() {
    // Get results table render array.
    $content = $this->adminListContent();

    // Create a styled button above the results table.
    $add_url = Url::fromRoute('labels.add_label');
    return [
      '#type' => 'markup',
      'action_links' => [
        '#type' => 'container',
        '#attributes' => [
          'class' => [
            'action-links',
          ],
        ],
        'add_button' => [
          '#type' => 'link',
          '#url' => $add_url,
          '#title' => $this->t('Add a Label'),
          '#attributes' => [
            'class' => [
              'button',
              'button-action',
              'button--primary',
            ],
          ],
        ],
      ],

      'content' => $content,
    ];
  }

  /**
   * DeleteLabel.
   *
   * @return RedirectResponse
   *
   *   Redirect to admin list.
   */
  public function deleteLabel($label) {
    // Get the config as a config object.
    $config = $this->configFactory->getEditable('labels.labels');

    // Get the labels as an array.
    $configData = $config->getRawData();

    // Delete the label from the array.
    unset($configData[$label]);

    // Set the array as the data of the config object.
    $config->setData($configData)->save();

    // Set success message and return to the admin list.
    drupal_set_message($this->t('Label deleted successfully.'));
    return new RedirectResponse(Url::fromRoute('labels.admin_list')->toString());
  }

  /**
   * AdminListContent.
   *
   * @return array
   *
   *   Returns a render array for results table.
   */
  private function adminListContent() {
    // Get the labels as an array.
    $config = $this->config('labels.labels')->getRawData();

    // Create a table render array of labels.
    $content = [
      '#type' => 'table',
      '#header' => [
        $this->t('Name'),
        $this->t('Label'),
        $this->t('Replacement'),
        $this->t('Themes'),
        $this->t('Hooks'),
        '',
      ],
      '#empty' => $this->t('There are no labels to display.'),
    ];

    // Add the rows.
    foreach ($config as $label => $data) {
      $replacement = '';
      if (isset($data['replacement'])) {
        $replacement = $data['replacement'];
      }

      // Create a delete Link for each row.
      $deleteUrl = Url::fromRoute('labels.delete_label', array('label' => $label));

      // Create an edit link for each row.
      $editUrl = Url::fromRoute('labels.edit_label', array('label' => $label));

      $actionsRenderArray = [
        '#type' => 'dropbutton',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => $editUrl,
          ],
          'delete' => [
            'title' => $this->t('Delete'),
            'url' => $deleteUrl,
          ],
        ],
      ];

      // Set all of the columns for the row.
      $content['#rows'][$label] = [
        'name' => $data['name'],
        'label' => $label,
        'replacement' => $replacement,
        'themes' => implode(', ', $data['themes']),
        'hooks' => implode(', ', $data['hooks']),
        'actions' => drupal_render($actionsRenderArray),
      ];
    }
    return $content;
  }

  /**
   * GetTwigSafeLabel.
   *
   * @return string
   *
   *   Converts labels into a twig-safe, consistent format.
   */
  static public function getTwigSafeLabel($label) {
    return 'label__' . $label;
  }

}
