Labels
----------------------
Author -  Grady Wring (https://www.drupal.org/u/gradywring)


Overview:
--------
This module provides developer-configurable labels that can be printed in any
twig template. Admin users can edit the text that will be output wherever the
label appears.

We often find ourselves providing custom blocks in order to allow site editors
and administrators a simple way to edit text on sites. In some cases, this is a
great solution. In other cases, it is overkill when a simple, editable
text-label would suffice.

Dependencies:
 None

Sponsored by:
<a href="http://coolbluei.com">Cool Blue Interactive</a>

Installation:
------------

1. Copy the labels directory to the Drupal /modules/ directory.

2. Go to Extend and enable the module.

3. Go to Configuration -> Labels and create the labels you need.

4. Go to Structure -> Label Replacements to configure the text displayed when
each label is rendered.

5. Place label placeholders in your twig like this {{ label__hello_world }}
where 'hello_world' is a label you created.

Last updated:
------------
09/07/2016
